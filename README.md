# skekb_to_madXsuite

Small repository containing an example on how to convert the simple SKEKB lattice into MAD-X and Xsuite.

Install SAD and run the `convert_*.sad` scripts to convert the lattices from the `lattice` directory.
The script to convert from SAD to MAD-X is in the `toolkit` directory, together with a macro to create a twiss.

Tests are including to check that the produced sequence can be loaded in MAD-X, and to compare the twiss of the different codes.
To run, set up a Python3.9 installation, install the required packages (`pip install -r python_requirements.txt`) and run `pytest --lattice=[hl]er ./tests/`.

In `tests/test_conversion.py`, there are additional tests that convert the MAD-X lattice to XSuite, perform slicing, and check that the optics remains consistent.
The sequences and twiss file are saved in the artifacts for [LER](https://gitlab.cern.ch/mihofer/skekb_to_madxsuite/builds/artifacts/master/browse?job=test_ler) and [HER](https://gitlab.cern.ch/mihofer/skekb_to_madxsuite/builds/artifacts/master/browse?job=test_her).

*Caveat*: The conversion script was hacked to remove the `FRINGE` and `F1/F2` property, as MAD-X can't handle these.
Optics comes out a bit distorted.

Helpful Links:  
<https://hep-project-sad.web.cern.ch/SADHelp/SADHelp.html> which is similar to <https://acc-physics.kek.jp/SAD/how-to-use-sad/sad-ffs-command-sad-script/>  
<https://www.overleaf.com/read/tnhmnswwgyyq>  
<https://conference-indico.kek.jp/event/75/contributions/1470/attachments/983/1042/SAD_exapmle_190918c.pdf>  
