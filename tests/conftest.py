import pytest

def pytest_addoption(parser):
    parser.addoption(
        "--lattice", action="store", default="ler", choices=['ler','her'], help="Which lattice to test, Choices: ler, her"
    )


@pytest.fixture
def lattice(request):
    return request.config.getoption("--lattice")