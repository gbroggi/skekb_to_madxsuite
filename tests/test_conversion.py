from pathlib import Path
import json
import pytest
from pandas.testing import assert_frame_equal
from numpy.testing import assert_allclose
from cpymad.madx import Madx
import tfs
import xtrack as xt
import xobjects as xo

SEQNAME = {
    'ler':'ASC',
    'her':'ASCE',
}

ENERGY = {
    'ler':7e9,
    'her':4e9,
}

def test_madx_loading(lattice):

    with Madx() as madx:
        madx.call(f'./seq_{lattice}.seq')
        madx.beam()
        madx.use(SEQNAME[lattice])
        madx.input("""
VOLTCAFFRP = 0.0;
VOLTCAEFRP = 0.0;
VOLTCADFRP = 0.0;
VOLTCACFRP = 0.0;
VOLTCABFRP = 0.0;
VOLTCAAFRP = 0.0;
VOLTCAAFLP = 0.0;
VOLTCABFLP = 0.0;
VOLTCACFLP = 0.0;
VOLTCADFLP = 0.0;
VOLTCAEFLP = 0.0;
VOLTCAFFLP = 0.0;
VOLTCAFORP = 0.0;
VOLTCAEORP = 0.0;
VOLTCADORP = 0.0;
VOLTCACORP = 0.0;
VOLTCABORP = 0.0;
VOLTCAAORP = 0.0;
                   
VOLTCADNRE = 0.0;
VOLTCACNRE = 0.0;
VOLTCABNRE = 0.0;
VOLTCAANRE = 0.0;
VOLTCAANLE = 0.0;
VOLTCABNLE = 0.0;
VOLTCACNLE = 0.0;
VOLTCADNLE = 0.0;
VOLTCAAOLE = 0.0;
VOLTCABOLE = 0.0;
VOLTCACOLE = 0.0;
VOLTCADOLE = 0.0;
VOLTCAEOLE = 0.0;
VOLTCAFOLE = 0.0;
VOLTCAGOLE = 0.0;
VOLTCAHOLE = 0.0;
""")
        madx.twiss(file=f'twiss_{lattice}_madx.tfs')


@pytest.mark.xfail(reason='Doesnt work due to fringe fields being neglected.')
def test_compare_twiss(lattice):

    sad_twiss = tfs.read(f'twiss_{lattice}_sad.tfs', index='NAME')
    madx_twiss = tfs.read(f'twiss_{lattice}_madx.tfs', index='NAME')

    sad_twiss = sad_twiss[sad_twiss.KEYWORD.str.contains('MARK')]
    madx_twiss = madx_twiss[madx_twiss.KEYWORD.str.contains('MARKER')]

    # somehow there is a marker mismatch
    assert_frame_equal(
        left=sad_twiss.loc[['IP.1', 'IP.2'],['BETX', 'BETY', 'ALFX', 'ALFY', 'DX']],
        right=madx_twiss.loc[['IP.1', 'IP.2'],['BETX', 'BETY', 'ALFX', 'ALFY', 'DX']],
    )


def test_xsuite_loading(lattice):

    with Madx() as madx:
        madx.call(f'./seq_{lattice}.seq')
        madx.beam()
        madx.use(SEQNAME[lattice])
        line_thick = xt.Line.from_madx_sequence(madx.sequence[SEQNAME[lattice]], allow_thick=True,
                                  deferred_expressions=True)
        line_thick.particle_ref = xt.Particles(mass0=xt.ELECTRON_MASS_EV,
                                 pc=ENERGY[lattice])
    line_thick.build_tracker()
    line_thick.to_json(f'seq_{lattice}.json')


# since mad-x and sad differ due to fringe fields, only compare mad-x and xsuite for now
def test_compare_xsuite_madx_twiss(lattice):

    line_thick=xt.Line.from_json(f'seq_{lattice}.json')
    line_thick.build_tracker()

    tw_thick_no_rad = line_thick.twiss(method='4d', only_markers=True)
    xsuite_twiss = tw_thick_no_rad.to_pandas()
    xsuite_twiss.to_csv(f'twiss_{lattice}_xsuite.csv')
    xsuite_twiss['name'] = xsuite_twiss['name'].str.upper()

    madx_twiss = tfs.read(f'twiss_{lattice}_madx.tfs')

    index_intersection = list(set(xsuite_twiss['name']) & set(madx_twiss['NAME']))
    
    assert_allclose(
        xsuite_twiss.loc[xsuite_twiss['name'].isin(index_intersection), ['betx', 'bety', 'alfx', 'alfy', 'dx']].to_numpy(),
        madx_twiss.loc[madx_twiss['NAME'].isin(index_intersection), ['BETX', 'BETY', 'ALFX', 'ALFY', 'DX']].to_numpy(),
        rtol=1e-5
    )


def test_xsuite_slicing(lattice):

    line_thick=xt.Line.from_json(f'seq_{lattice}.json')
    line_thick.build_tracker()
    tw_thick_no_rad = line_thick.twiss(method='4d')
    
    line = line_thick.copy()
    Strategy = xt.slicing.Strategy
    Teapot = xt.slicing.Teapot
    slicing_strategies = [
        Strategy(slicing=Teapot(1)),  # Default catch-all as in MAD-X
        Strategy(slicing=Teapot(3), element_type=xt.Bend),
        Strategy(slicing=Teapot(3), element_type=xt.CombinedFunctionMagnet),
        Strategy(slicing=Teapot(50), element_type=xt.Quadrupole), # Starting point, refine later
        Strategy(slicing=Teapot(50), name=r'^qc.*'),
    ]

    line.slice_thick_elements(slicing_strategies=slicing_strategies)
    line.build_tracker()
    tw_thin_before = line.twiss(ele_start=0, ele_stop=len(line)-1, method='4d',
                            twiss_init=tw_thick_no_rad.get_twiss_init(0))
    line.to_json(f'seq_{lattice}_thin.json')


def test_compare_xsuite_thick_thin_twiss(lattice):

    line_thick=xt.Line.from_json(f'seq_{lattice}.json')
    line_thick.build_tracker()
    line=xt.Line.from_json(f'seq_{lattice}_thin.json')
    line.build_tracker()

    tw_thick_no_rad = line_thick.twiss(method='4d', only_markers=True)
    xsuite_thick_twiss = tw_thick_no_rad.to_pandas()

    tw_thin_no_rad = line_thick.twiss(method='4d', only_markers=True)
    xsuite_thin_twiss = tw_thin_no_rad.to_pandas()
    
    assert_allclose(
        xsuite_thick_twiss.loc[:, ['betx', 'bety', 'alfx', 'alfy', 'dx']].to_numpy(),
        xsuite_thin_twiss.loc[:, ['betx', 'bety', 'alfx', 'alfy', 'dx']].to_numpy(),
        rtol=1e-5
    )
